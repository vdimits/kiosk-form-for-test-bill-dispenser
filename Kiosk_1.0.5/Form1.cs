﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO.Ports;
using System.IO;
using NDE1000;

namespace Kiosk_1._0._5
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        NDE n = new NDE();
        

        private void Form1_Load(object sender, EventArgs e)
        {
            btnClose.Enabled = false;
            btnOpen.Enabled = true;
        }


        
        
        private void btnOpen_Click(object sender, EventArgs e)
        {

            n.serialPort("COM4");
            n.OpenPort();
            btnOpen.Enabled = false;
            btnClose.Enabled = true;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            n.ClosePort();
            btnClose.Enabled = false;
            btnOpen.Enabled = true;
        }

        // CMD = command
        // CMDC = command clear

        

        private void btnClearDisNum_Click(object sender, EventArgs e)
        {
            string b = "";
            string c = "";
            n.ClearDispenseNumber(ref b, ref c);
            rtbResponse.Text += b + " " + c + "\n";
        }

        

        private void btnClearErrMsg_Click(object sender, EventArgs e)
        {
            string b = "";
            string c = "";
            n.ClearErrorMessage(ref b, ref c);
            rtbResponse.Text += b + " " + c + "\n";
        }
        
        private void btnDispense_Click(object sender, EventArgs e)
        {
            //string jmlh = textBox1.Text;
            //rtbResponse.Text += n.Dispense(jmlh) + "\n";
        }

        private void btnCheckDisNum_Click(object sender, EventArgs e)
        {
            string b = "";
            string c = "";
            n.ClearBoth(ref b, ref c);
            rtbResponse.Text += b + " " + c + "\n"; 
        }
        
        private void btnClearBoth_Click(object sender, EventArgs e)
        {
            //rtbResponse.Text += n.ClearBoth() + "\n";
        }

        private void btnLock_Click(object sender, EventArgs e)
        {
            //rtbResponse.Text += n.Lock() + "\n";
        }

        private void btnCheckStatus_Click(object sender, EventArgs e)
        {
            //rtbResponse.Text += n.CheckStatus() + "\n";
        }

        private void btnWriteDate_Click(object sender, EventArgs e)
        {
            n.WriteDate();
        }

        private void btnWriteTime_Click(object sender, EventArgs e)
        {
            n.WriteTime();
        }

        private void btnReadDate_Click(object sender, EventArgs e)
        {
            //rtbResponse.Text += n.ReadDate() + "\n";
        }

        private void btnReadTime_Click(object sender, EventArgs e)
        {
            //rtbResponse.Text += n.ReadTime() + "\n";
        }
    }
}
