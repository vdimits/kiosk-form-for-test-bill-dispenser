﻿namespace Kiosk_1._0._5
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnClearDisNum = new System.Windows.Forms.Button();
            this.btnClearErrMsg = new System.Windows.Forms.Button();
            this.btnClearBoth = new System.Windows.Forms.Button();
            this.btnLock = new System.Windows.Forms.Button();
            this.btnCheckStatus = new System.Windows.Forms.Button();
            this.btnCheckDisNum = new System.Windows.Forms.Button();
            this.btnWriteDate = new System.Windows.Forms.Button();
            this.btnWriteTime = new System.Windows.Forms.Button();
            this.btnReadDate = new System.Windows.Forms.Button();
            this.btnReadTime = new System.Windows.Forms.Button();
            this.btnDispense = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.btnOpen = new System.Windows.Forms.Button();
            this.btnClose = new System.Windows.Forms.Button();
            this.rtbResponse = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnClearDisNum
            // 
            this.btnClearDisNum.Location = new System.Drawing.Point(9, 10);
            this.btnClearDisNum.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearDisNum.Name = "btnClearDisNum";
            this.btnClearDisNum.Size = new System.Drawing.Size(124, 63);
            this.btnClearDisNum.TabIndex = 0;
            this.btnClearDisNum.Text = "Clear Dispense Number";
            this.btnClearDisNum.UseVisualStyleBackColor = true;
            this.btnClearDisNum.Click += new System.EventHandler(this.btnClearDisNum_Click);
            // 
            // btnClearErrMsg
            // 
            this.btnClearErrMsg.Location = new System.Drawing.Point(9, 78);
            this.btnClearErrMsg.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearErrMsg.Name = "btnClearErrMsg";
            this.btnClearErrMsg.Size = new System.Drawing.Size(124, 63);
            this.btnClearErrMsg.TabIndex = 0;
            this.btnClearErrMsg.Text = "Clear Error Message";
            this.btnClearErrMsg.UseVisualStyleBackColor = true;
            this.btnClearErrMsg.Click += new System.EventHandler(this.btnClearErrMsg_Click);
            // 
            // btnClearBoth
            // 
            this.btnClearBoth.Location = new System.Drawing.Point(9, 146);
            this.btnClearBoth.Margin = new System.Windows.Forms.Padding(2);
            this.btnClearBoth.Name = "btnClearBoth";
            this.btnClearBoth.Size = new System.Drawing.Size(124, 63);
            this.btnClearBoth.TabIndex = 0;
            this.btnClearBoth.Text = "Clear Both";
            this.btnClearBoth.UseVisualStyleBackColor = true;
            this.btnClearBoth.Click += new System.EventHandler(this.btnClearBoth_Click);
            // 
            // btnLock
            // 
            this.btnLock.Location = new System.Drawing.Point(9, 219);
            this.btnLock.Margin = new System.Windows.Forms.Padding(2);
            this.btnLock.Name = "btnLock";
            this.btnLock.Size = new System.Drawing.Size(124, 63);
            this.btnLock.TabIndex = 0;
            this.btnLock.Text = "Lock";
            this.btnLock.UseVisualStyleBackColor = true;
            this.btnLock.Click += new System.EventHandler(this.btnLock_Click);
            // 
            // btnCheckStatus
            // 
            this.btnCheckStatus.Location = new System.Drawing.Point(9, 292);
            this.btnCheckStatus.Margin = new System.Windows.Forms.Padding(2);
            this.btnCheckStatus.Name = "btnCheckStatus";
            this.btnCheckStatus.Size = new System.Drawing.Size(124, 63);
            this.btnCheckStatus.TabIndex = 0;
            this.btnCheckStatus.Text = "Check Status";
            this.btnCheckStatus.UseVisualStyleBackColor = true;
            this.btnCheckStatus.Click += new System.EventHandler(this.btnCheckStatus_Click);
            // 
            // btnCheckDisNum
            // 
            this.btnCheckDisNum.Location = new System.Drawing.Point(137, 10);
            this.btnCheckDisNum.Margin = new System.Windows.Forms.Padding(2);
            this.btnCheckDisNum.Name = "btnCheckDisNum";
            this.btnCheckDisNum.Size = new System.Drawing.Size(124, 63);
            this.btnCheckDisNum.TabIndex = 0;
            this.btnCheckDisNum.Text = "Check Dispensed Number";
            this.btnCheckDisNum.UseVisualStyleBackColor = true;
            this.btnCheckDisNum.Click += new System.EventHandler(this.btnCheckDisNum_Click);
            // 
            // btnWriteDate
            // 
            this.btnWriteDate.Location = new System.Drawing.Point(137, 78);
            this.btnWriteDate.Margin = new System.Windows.Forms.Padding(2);
            this.btnWriteDate.Name = "btnWriteDate";
            this.btnWriteDate.Size = new System.Drawing.Size(124, 63);
            this.btnWriteDate.TabIndex = 0;
            this.btnWriteDate.Text = "Write Date";
            this.btnWriteDate.UseVisualStyleBackColor = true;
            this.btnWriteDate.Click += new System.EventHandler(this.btnWriteDate_Click);
            // 
            // btnWriteTime
            // 
            this.btnWriteTime.Location = new System.Drawing.Point(137, 146);
            this.btnWriteTime.Margin = new System.Windows.Forms.Padding(2);
            this.btnWriteTime.Name = "btnWriteTime";
            this.btnWriteTime.Size = new System.Drawing.Size(124, 63);
            this.btnWriteTime.TabIndex = 0;
            this.btnWriteTime.Text = "Write Time";
            this.btnWriteTime.UseVisualStyleBackColor = true;
            this.btnWriteTime.Click += new System.EventHandler(this.btnWriteTime_Click);
            // 
            // btnReadDate
            // 
            this.btnReadDate.Location = new System.Drawing.Point(137, 219);
            this.btnReadDate.Margin = new System.Windows.Forms.Padding(2);
            this.btnReadDate.Name = "btnReadDate";
            this.btnReadDate.Size = new System.Drawing.Size(124, 63);
            this.btnReadDate.TabIndex = 0;
            this.btnReadDate.Text = "Read Date";
            this.btnReadDate.UseVisualStyleBackColor = true;
            this.btnReadDate.Click += new System.EventHandler(this.btnReadDate_Click);
            // 
            // btnReadTime
            // 
            this.btnReadTime.Location = new System.Drawing.Point(137, 292);
            this.btnReadTime.Margin = new System.Windows.Forms.Padding(2);
            this.btnReadTime.Name = "btnReadTime";
            this.btnReadTime.Size = new System.Drawing.Size(124, 63);
            this.btnReadTime.TabIndex = 0;
            this.btnReadTime.Text = "Read Time";
            this.btnReadTime.UseVisualStyleBackColor = true;
            this.btnReadTime.Click += new System.EventHandler(this.btnReadTime_Click);
            // 
            // btnDispense
            // 
            this.btnDispense.Location = new System.Drawing.Point(276, 292);
            this.btnDispense.Margin = new System.Windows.Forms.Padding(2);
            this.btnDispense.Name = "btnDispense";
            this.btnDispense.Size = new System.Drawing.Size(124, 63);
            this.btnDispense.TabIndex = 0;
            this.btnDispense.Text = "Dispense";
            this.btnDispense.UseVisualStyleBackColor = true;
            this.btnDispense.Click += new System.EventHandler(this.btnDispense_Click);
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(276, 264);
            this.textBox1.Margin = new System.Windows.Forms.Padding(2);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(124, 20);
            this.textBox1.TabIndex = 1;
            // 
            // btnOpen
            // 
            this.btnOpen.Location = new System.Drawing.Point(295, 23);
            this.btnOpen.Margin = new System.Windows.Forms.Padding(2);
            this.btnOpen.Name = "btnOpen";
            this.btnOpen.Size = new System.Drawing.Size(98, 50);
            this.btnOpen.TabIndex = 0;
            this.btnOpen.Text = "Open";
            this.btnOpen.UseVisualStyleBackColor = true;
            this.btnOpen.Click += new System.EventHandler(this.btnOpen_Click);
            // 
            // btnClose
            // 
            this.btnClose.Location = new System.Drawing.Point(295, 91);
            this.btnClose.Margin = new System.Windows.Forms.Padding(2);
            this.btnClose.Name = "btnClose";
            this.btnClose.Size = new System.Drawing.Size(98, 50);
            this.btnClose.TabIndex = 0;
            this.btnClose.Text = "Close";
            this.btnClose.UseVisualStyleBackColor = true;
            this.btnClose.Click += new System.EventHandler(this.btnClose_Click);
            // 
            // rtbResponse
            // 
            this.rtbResponse.Location = new System.Drawing.Point(422, 10);
            this.rtbResponse.Margin = new System.Windows.Forms.Padding(2);
            this.rtbResponse.Name = "rtbResponse";
            this.rtbResponse.Size = new System.Drawing.Size(170, 347);
            this.rtbResponse.TabIndex = 2;
            this.rtbResponse.Text = "";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(600, 366);
            this.Controls.Add(this.rtbResponse);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.btnReadTime);
            this.Controls.Add(this.btnCheckStatus);
            this.Controls.Add(this.btnReadDate);
            this.Controls.Add(this.btnLock);
            this.Controls.Add(this.btnWriteTime);
            this.Controls.Add(this.btnClearBoth);
            this.Controls.Add(this.btnWriteDate);
            this.Controls.Add(this.btnOpen);
            this.Controls.Add(this.btnClose);
            this.Controls.Add(this.btnDispense);
            this.Controls.Add(this.btnCheckDisNum);
            this.Controls.Add(this.btnClearErrMsg);
            this.Controls.Add(this.btnClearDisNum);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnClearDisNum;
        private System.Windows.Forms.Button btnClearErrMsg;
        private System.Windows.Forms.Button btnClearBoth;
        private System.Windows.Forms.Button btnLock;
        private System.Windows.Forms.Button btnCheckStatus;
        private System.Windows.Forms.Button btnCheckDisNum;
        private System.Windows.Forms.Button btnWriteDate;
        private System.Windows.Forms.Button btnWriteTime;
        private System.Windows.Forms.Button btnReadDate;
        private System.Windows.Forms.Button btnReadTime;
        private System.Windows.Forms.Button btnDispense;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button btnOpen;
        private System.Windows.Forms.Button btnClose;
        private System.Windows.Forms.RichTextBox rtbResponse;
    }
}

